<?php

/**
 * Utilisations de pipelines par Edition_directe.
 *
 * @plugin     Edition_directe
 * @copyright  2011 - 2022
 * @author     Rainer Müller
 * @licence    GNU/GPL
 * @package    SPIP\Edition_directe\Pipelines
 */


// styles
function edition_directe_header_prive($flux) {
	$flux .= '<link rel="stylesheet" href="' . find_in_path('css/edition_directe_styles.css') . '" type="text/css" media="all" />';

	return $flux;
}

// Ajouter le formulaire upload
function edition_directe_affiche_gauche($flux) {
	include_spip('edition_directe_fonctions');

	$objets_edition_directe = objets_edition_directe();
	if ($en_cours = trouver_objet_exec($flux['args']['exec']) and $type = $en_cours['type'] and in_array($type, $objets_edition_directe) and $id_table_objet = $en_cours['id_table_objet'] and ($id = intval($flux['args'][$id_table_objet]) or $id = 0 - $GLOBALS['visiteur_session']['id_auteur']) and autoriser('joindredocument', $type, $id)) {

		if ($id > 0)
			$flux['data'] .= recuperer_fond('prive/objets/editer/colonne_document', array (
				'objet' => $type,
				'id_objet' => $id
			));
	}

	return $flux;
}

// Modifié la page de l'objet
function edition_directe_recuperer_fond($flux) {
	include_spip('edition_directe_fonctions');
	include_spip('inc/autoriser');
	$fond = $flux['args']['fond'];
	$contexte = isset($flux['args']['contexte']) ? $flux['args']['contexte'] : array ();
	$texte = $flux['data']['texte'];

	// Seulement dans l'espace priv&eacute;
	if (test_espace_prive() and
		$exec =_request('exec') and
		$objet_exec = trouver_objet_exec($exec) and
		$objet = $objet_exec['type'] and
		$id = $objet_exec['id_table_objet'] and
		$contexte['objet'] = $objet and
		isset($contexte[$id]) and
		$contexte['id_objet'] = $contexte[$id] and
		autoriser('modifier', $objet, $contexte['id_objet'])) {
		// On cherche les objets actifs pour l'édition directe
		$objets = objets_edition_directe();

		// Les objets éditables déclarés
		$objets_dispos = lister_objets(array ());

		// Insertion du formulaire d'édition
		if (in_array($objet, $objets)) {
			if ($fond == 'prive/squelettes/contenu/' . $objet) {
				$edition = recuperer_fond('prive/echafaudage/contenu/objet_edit_directe', $contexte, array (
					'ajax' => true
				));
				$icone = '
					<span class="icone_edition_directe icone active">
						<a class="ajax" href="' . generer_action_auteur('edition_directe_auteur', 'inactive-' . $objet, generer_url_ecrire($objet, $id . '=' . $contexte['id_objet'], false)) . '" title="' . _T('edir:desactiver_edition_directe_objet') . $objet . '">
							<img src="' . find_in_path('prive/themes/spip/images/edir-24.png') . '"/>
							<b>' . _T('edir:titre_plugin') . '</b>
						</a>
					</span>';
				$patterns = array (
					'/class=\'icone/',
					'/<!--\/hd-->/',
					'/<h1(.*?)>/',
				);

				$replacements = array (
					'class="icone invisible',
					$edition . '<!--/hd-->',
					$icone . '<h1 $1>',

				);
				$flux['data']['texte'] = preg_replace($patterns, $replacements, $texte, 1);
			}
			// Suppression de la prévisualisation
			if ($fond == 'prive/objets/contenu/' . $objet) {
				$flux['data']['texte'] = '';
			}
		}
		elseif ($fond == 'prive/squelettes/contenu/' . $objet and in_array($objet, $objets_dispos)) {
			$icone = '
					<span class="icone_edition_directe icone inactive">
						<a class="ajax" href="' . generer_action_auteur('edition_directe_auteur', 'active-' . $objet, generer_url_ecrire($objet, $id . '=' . $contexte['id_objet'], false)) . '" title="' . _T('edir:activer_edition_directe_objet') . $objet . '">
							<img src="' . find_in_path('prive/themes/spip/images/edir-24.png') . '"/>
							<b>' . _T('edir:titre_plugin') . '</b>
						</a>
					</span>';
			$patterns = array (
				'/<h1(.*?)>/',
			);
			$replacements = array (
				$icone . '<h1 $1>',
			);
			$flux['data']['texte'] = preg_replace($patterns, $replacements, $texte, 1);
		}
	}

	return $flux;
}

// Gérer le retour après validation du formulaire
function edition_directe_formulaire_traiter($flux) {
	$objets = objets_edition_directe();
	$form = $flux['args']['form'];
	$objet = str_replace('editer_', '', $form);

	if (in_array($objet, $objets) and ! $_REQUEST['redirect'] and _request('exec')) {
		if ($objet == 'site') {
			$id_objet = $flux['data']['id_syndic'];
			$flux['data']['redirect'] = generer_url_ecrire($objet, 'id_syndic=' . $id_objet);
		}
		elseif ($id_objet = $flux['data']['id_' . $objet])
			$flux['data']['redirect'] = generer_url_ecrire($objet, 'id_' . $objet . '=' . $id_objet);
	}

	return $flux;
}
