<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/edir?lang_cible=fa
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'activer_edition_directe' => 'فعال سازي ويرايش مستقيم براي: ',

	// T
	'titre_plugin' => 'ويرايش مستقيم',

	// V
	'vider_cache' => 'لطفاً حافظه‌ي دم‌دستي را خالي كنيد'
);
