<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/edir?lang_cible=nl
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'activer_edition_directe' => 'Activeer directe aanpassing voor:',
	'activer_edition_directe_objet' => 'Activeer directe aanpassing voor het object',

	// D
	'desactiver_edition_directe_objet' => 'Stop directe aanpassing van het object',

	// T
	'titre_plugin' => 'Directe aanpassing',

	// V
	'vider_cache' => 'Maak de cache leeg'
);
