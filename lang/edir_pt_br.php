<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/edir?lang_cible=pt_br
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'activer_edition_directe' => 'Ativar a edição direta para:',
	'activer_edition_directe_objet' => 'Ativar a edição direta para o objeto',

	// D
	'desactiver_edition_directe_objet' => 'Desativar a edição direta para o objeto',

	// T
	'titre_plugin' => 'Edição direta',

	// V
	'vider_cache' => 'Por favor, limpe o cache'
);
