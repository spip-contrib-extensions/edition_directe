<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-edition_directe?lang_cible=nl
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'edition_directe_description' => 'SPIP objecten direct aanpassen vanaf hun pagina in de privé-omgeving. De direct aan te passen objecten moeten in de configuratie worden geactiveerd',
	'edition_directe_slogan' => 'Pas SPIP objecten direct aan vanaf hun pagina'
);
