<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-edition_directe?lang_cible=pt_br
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'edition_directe_description' => 'Editar diretamente os objetos SPIP a partir das suas páginas, na área restrita. Os objetos a serem editados diretamente devem ser ativados na Configuração',
	'edition_directe_slogan' => 'Editar diretamente os objetos SPIP a partir da sua página'
);
